// 2. Create a function that will generate and return new user data(email, password(13), firstName, lastName, phoneNumber)
// The idea is in generating random value for each property, for ex.
// first run of generateNewUser() return this object
// {
//     email: "zjlmfclq@gmail.com",
//     firstName: "pqflsz",
//     lastName: "amwodthw",
//     password: "]Tp[7Sxd0B^4,",
//     phoneNumber: "(721) 354-0698"
// }
// second run of generateNewUser() return this object
// {
//     email: "asdaswerafg@gmail.com",
//     firstName: "asfjnagfs",
//     lastName: "asdanswjn",
//     password: "frbnefbajnsakw[",
//     phoneNumber: "(555) 555-1234"
// }

// So each time new user will be a new one with some random values.
// You can use phone number format that fit your country template.
// For email it is up to you, but there should be @ and . to be similar for real email.

// And again, it is additional task, you can make it complicated, for ex. make first letter in first and last name in upper case.
// Play with it, the goal is to find the solution with your current background.

export function generateNewUser() {
    // Wright your code here
}
