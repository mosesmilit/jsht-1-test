import { load } from "../utils/utils.js";
import { validateData } from "../tasks/t3.js";

describe("validateData", () => {
    test("should return true if the data load contains the specified key and value", () => {
        const key = "product";
        const value = ["test00000"];

        const result = validateData(key, value, load);

        expect(result).toBe(true);
    });

    test("should return false if the data load does not contain the specified key and value", () => {
        const key = "product";
        const value = ["test12345"];

        const result = validateData(key, value, load);

        expect(result).toBe(false);
    });

    test("should return false if the key is present but the value does not match", () => {
        const key = "product";
        const value = ["test00001"];

        const result = validateData(key, value, load);

        expect(result).toBe(false);
    });

    test("should return false if the key is not present in any of the data load entries", () => {
        const key = "nonexistentKey";
        const value = ["test00000"];

        const result = validateData(key, value, load);

        expect(result).toBe(false);
    });
});
