import { generateNewUser } from "../tasks/t2.js";

const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

describe("Generate random User object", () => {
    test("should return a new user object with random values", () => {
        const user = generateNewUser();

        expect(user).toMatchObject({
            email: expect.stringMatching(emailRegex),
            firstName: expect.any(String),
            lastName: expect.any(String),
            password: expect.any(String),
            phoneNumber: expect.any(Number),
        });
        expect(user.password.length).toBeGreaterThan(12);
    });

    test("should generate different user objects on multiple runs", () => {
        const user1 = generateNewUser();
        const user2 = generateNewUser();

        expect(user1).not.toEqual(user2);
    });
});
