export const value = ["test00000"];

const window = {};

export const load = [
    {
        text: {
            type: "requestTestData",
            payload: { testAction: "exit" },
        },
        testResponse: null,
    },
    {
        text: {
            type: "requestTestData",
            callback: window.submitTest,
            testCallback: window.testError,
        },
        testResponse: null,
    },
    {
        text: {
            type: "requestTestData",
            payload: { product: ["test00000"] },
            testCallback: window.submitTest,
        },
        testResponse: null,
    },
    { text: { type: "test" }, response: null },
];
