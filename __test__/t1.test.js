import { getNextWorkingDate } from "../tasks/t1.js";

const oneDayMil = 8.64e7;
const twoDaysMil = 1.728e8;
const threeDaysMil = 2.592e8;

const Tuesday = "2023-08-01T17:12:04.588Z";
const Friday = "2023-08-04T17:12:04.588Z";
const Sunday = "2023-08-05T17:12:04.588Z";
const Saturday = "2023-08-06T17:12:04.588Z";

describe("getNextWorkingDate", () => {
    afterEach(() => {
        jest.useRealTimers();
    });

    describe("If today is Friday.", () => {
        test("Should return Monday in UTC format.", () => {
            const today = new Date(Friday);
            const expectedNextWorkingDate = new Date(today.getTime() + threeDaysMil).toUTCString();

            jest.useFakeTimers("modern");
            jest.setSystemTime(today);

            const result = getNextWorkingDate();

            expect(result.split(" ").slice(0, 4).join(" ")).toBe(
                expectedNextWorkingDate.split(" ").slice(0, 4).join(" ")
            );
        });
    });

    describe("If today is Sunday.", () => {
        test("Should return Monday in UTC format.", () => {
            const today = new Date(Sunday);
            const expectedNextWorkingDate = new Date(today.getTime() + twoDaysMil).toUTCString();

            jest.useFakeTimers("modern");
            jest.setSystemTime(today);

            const result = getNextWorkingDate();

            expect(result.split(" ").slice(0, 4).join(" ")).toBe(
                expectedNextWorkingDate.split(" ").slice(0, 4).join(" ")
            );
        });
    });

    describe("If today is Saturday.", () => {
        test("Should return Monday in UTC format.", () => {
            const today = new Date(Saturday);
            const expectedNextWorkingDate = new Date(today.getTime() + oneDayMil).toUTCString();

            jest.useFakeTimers("modern");
            jest.setSystemTime(today);

            const result = getNextWorkingDate();

            expect(result.split(" ").slice(0, 4).join(" ")).toBe(
                expectedNextWorkingDate.split(" ").slice(0, 4).join(" ")
            );
        });
    });

    describe("Every day except Friday, Sunday and Saturday.", () => {
        test("Should return next working day in UTC format.", () => {
            const today = new Date(Tuesday);
            const expectedNextWorkingDate = new Date(today.getTime() + oneDayMil).toUTCString();

            jest.useFakeTimers("modern");
            jest.setSystemTime(today);

            const result = getNextWorkingDate();

            expect(result.split(" ").slice(0, 4).join(" ")).toBe(
                expectedNextWorkingDate.split(" ").slice(0, 4).join(" ")
            );
        });
    });
});
