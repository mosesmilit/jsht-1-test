# JS HT-1

## How to do this tasks?

1. Clone this repo

```cmd
git clone https://gitlab.com/mosesmilit/jsht-1-test.git
```

2. Go to cloned repo folder by command

```cmd
cd jsht-1-test
```

3.  Run command

```cmd
npm i
```

4. Go to tasks folder finish tasks and run command

```cmd
npm run test
```

5. Celebrate your success 🎉😎

# Tasks

1. Create a function that will return date in UTC format getNextWorkingDate **(excluding Saturday and Sunday)**, **Example**: if today is 07/28/2023 which is Friday function should return 07/31/2023 which is Monday in UTC format
2. Create a function that will generate and return new user data(email, password(13), firstName, lastName, phoneNumber).  
   The idea is in generating random value for each property, for ex.

-   First run of **generateNewUser()** should return this object

```js
{
    email: "zjlmfclq@gmail.com",
    firstName: "pqflsz",
    lastName: "amwodthw",
    password: "]Tp[7Sxd0B^4,",
    phoneNumber: "(721) 354-0698"
}
```

-   Second run of **generateNewUser()** should return this object

```js
{
    email: "asdaswerafg@gmail.com",
    firstName: "asfjnagfs",
    lastName: "asdanswjn",
    password: "frbnefbajnsakw[",
    phoneNumber: "(555) 555-1234"
}
```

-   So each time new user will be a new one with some random values.
    You can use phone number format that fit your country template.
    For email it is up to you, but there should be @ and . to be similar for real email.
-   And again, it is additional task, you can make it complicated, for ex. make first letter in first and last name in upper case.
    Play with it, the goal is to find the solution with your current background.

3. Create a function that returns true if the data load contain key value

**Example of key & value**

```js
product: ["test00000"];
```

## Data load example

```js
[
    {
        text: {
            type: "requestTestData",
            payload: { testAction: "exit" },
        },
        testResponse: null,
    },
    {
        text: {
            type: "requestTestData",
            callback: window.submitTest,
            testCallback: window.testError,
        },
        testResponse: null,
    },
    {
        text: {
            type: "requestTestData",
            payload: { product: ["test00000"] },
            testCallback: window.submitTest,
        },
        testResponse: null,
    },
    { text: { type: "test" }, response: null },
];
```
